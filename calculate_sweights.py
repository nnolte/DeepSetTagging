import sys
import pandas as pd
import numpy as np
import ROOT as R
from ROOT import RooFit
from plotting import plot_pulls
from IPython import embed
import uproot3 as u

mass = np.load(sys.argv[1])['B_M']

rfile = "mass.root"

branchdict = {"M": "float32"}
t = u.newtree(branchdict)
f = u.recreate(rfile)
f['t'] = t
f['t'].extend({"M" : mass})

tfile = R.TFile(rfile)
ttree = tfile.Get('t')
ttree.SetBranchStatus("*", 0)
ttree.SetBranchStatus("M", 1)

M = R.RooRealVar("M", "", 5149.99, 5450)

data = R.RooDataSet("dataset", "", ttree, R.RooArgSet(M))

# Signal B0
mu = R.RooRealVar("mu", "", 5279, 5275, 5285)
s1 = R.RooRealVar("s1", "", 12, 10, 20)
s2 = R.RooRealVar("s2", "", 14, 5, 20)
s3 = R.RooRealVar("s3", "", 10, 5, 20)

G1 = R.RooGaussian("G1", "", M, mu, s1)
G2 = R.RooGaussian("G2", "", M, mu, s2)
G3 = R.RooGaussian("G3", "", M, mu, s3)

f1 = R.RooRealVar("f1", "", 0.2, 0, 1)
f2 = R.RooRealVar("f2", "", 0.5, 0, 1)

signal = R.RooAddPdf("signal", "", R.RooArgList(G1, G2), R.RooArgList(f1), True)

# Backgrounds
# Bs
muBs = R.RooFormulaVar("muBs", "@0+87.45", R.RooArgList(mu))
G1s = R.RooGaussian("G1s", "", M, muBs, s1)
G2s = R.RooGaussian("G2s", "", M, muBs, s2)
G3s = R.RooGaussian("G3s", "", M, muBs, s3)
BsPeak = R.RooAddPdf("BsPeak", "", R.RooArgList(G1s, G2s, G3s), R.RooArgList(f1, f2), True)

# Combinatorial
E = R.RooRealVar("E", "", -0.001, -0.1, -0.0001)
combinatorial = R.RooExponential("combinatorial", "", M, E)

fb1 = R.RooRealVar("fb1", "", 0.99, 0.95, 1)

# Partial
s_p = R.RooRealVar("s_p", "", 20, 10, 60)
mu_p = R.RooRealVar("mu_p", "", 5160, 5150, 5179)
G_p  = R.RooGaussian("G_p", "", M, mu_p, s_p)

f_p  = R.RooRealVar("f_p", "", 0.1, 0, 1)

background = R.RooAddPdf("bkg", "", R.RooArgList(combinatorial, BsPeak, G_p), R.RooArgList(fb1, f_p), True)

# Combine
embed()
N = ttree.GetEntriesFast()
Nsig = R.RooRealVar("Nsig", "", N*0.08, 0, N)
Nbkg = R.RooRealVar("Nbkg", "", N*0.92, 0, N)
model = R.RooAddPdf("model", "", R.RooArgList(signal, background), R.RooArgList(Nsig, Nbkg))

# Fit
model.fitTo(data, RooFit.NumCPU(20), RooFit.Extended(True))

# sWeights
sData = R.RooStats.SPlot("sData", "", data, model, R.RooArgList(Nsig, Nbkg))
sWeights_signal = np.array([sData.GetSWeight(j, "Nsig") for j in range(N)])
sWeights_bkg    = np.array([sData.GetSWeight(j, "Nbkg") for j in range(N)])

np.savez("sweights.npz", sweight_s=sWeights_signal, sweights_b=sWeights_bkg)

# Plot
frame = M.frame()
data.plotOn(frame)
model.plotOn(frame, RooFit.Components("signal"), RooFit.LineColor(R.kRed))
model.plotOn(frame, RooFit.Components("BsPeak"), RooFit.LineColor(R.kRed))
model.plotOn(frame, RooFit.Components("G1"), RooFit.LineColor(R.kOrange))
model.plotOn(frame, RooFit.Components("G2"), RooFit.LineColor(R.kOrange))
model.plotOn(frame, RooFit.Components("G3"), RooFit.LineColor(R.kOrange))
model.plotOn(frame, RooFit.Components("bkg"), RooFit.LineColor(R.kBlue))
model.plotOn(frame, RooFit.Components("G_p"), RooFit.LineColor(R.kMagenta))
model.plotOn(frame, RooFit.LineColor(R.kBlack))

plot_pulls("Mplot", frame, spacing=True, save_path=".", pullstyle="flat")
