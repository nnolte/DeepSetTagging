#!/usr/bin/env python
# coding: utf-8

import argparse
import numpy as np
import torch
import joblib
from model import BaselineModel
import uproot3


def apply(data, model_fname, scaler_fname, out_fname):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    features = data["features"]
    evt_borders = data["evt_borders"]
    assert evt_borders[-1] == len(features)

    # features[features[:, 3] == -1, 3] = 0

    scaler = joblib.load(f"{scaler_fname}")
    features = scaler.transform(features)

    borders = np.array(list(zip(evt_borders[:-1], evt_borders[1:])))
    idx_vec = np.zeros(len(features), dtype=np.int64)
    for i, (b, e) in enumerate(borders):
        idx_vec[b:e] = i

    feat = torch.tensor(features).to(device)
    idx = torch.tensor(idx_vec).to(device)
    breakpoint()

    batch_size = 1000
    batch_borders = [
        (x[0, 0], x[-1, 1]) for x in np.array_split(borders, len(borders) // batch_size)
    ]

    model = BaselineModel(model_fname).to(device)
    model.eval()

    mypreds = np.zeros((len(borders), 1))

    for (beg, end) in batch_borders:
        tmp_data = feat[beg:end]
        # indices for the index_add inside the forward()
        tmp_idx = idx[beg:end] - idx[beg]

        e_beg, e_end = idx[[beg, end - 1]]
        # one past the last event is the boundary
        e_end += 1

        with torch.no_grad():
            output = model(tmp_data, tmp_idx)

        mypreds[e_beg:e_end] = torch.sigmoid(output).detach().cpu().numpy()

    pred_tags = mypreds.squeeze()
    eta = np.where(pred_tags > 0.5, 1 - pred_tags, pred_tags)
    epm_tags = np.where(pred_tags > 0.5, 1, -1).astype(np.int32)

    with uproot3.recreate(f"{out_fname}.root", compression=None) as file:
        file["DecayTree"] = uproot3.newtree(
            {
                "B_TRUEID": np.int32,
                "tag": np.int32,
                "eta": np.float64,
                "pred" : np.float64,
            }
        )
        t = file["DecayTree"]

        t["B_TRUEID"].newbasket(data["B_TRUEID"].astype(np.int32))
        t["tag"].newbasket(epm_tags.astype(np.int32))
        t["eta"].newbasket(eta.astype(np.float64))
        t["pred"].newbasket(pred_tags.astype(np.float64))


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Apply Flavour Tagging model to data.")
    parser.add_argument(
        "filename", help="File to apply the flavour tagging to (*.npz file)."
    )
    parser.add_argument(
        "-model-fname",
        default="model.pt",
        help="File name to load model weights from. Default is model.pt",
    )
    parser.add_argument(
        "-scaler-fname",
        default=None,
        help="File name to load scaler from. Default is MODELNAME_scaler.bin",
    )
    parser.add_argument(
        "-out-fname",
        default=None,
        help="Filename of root file output",
    )

    args = parser.parse_args()

    if args.out_fname == None:
        args.out_fname = args.filename[:-4] + "__" + args.model_fname[:-3]

    if args.scaler_fname == None:
        args.scaler_fname = args.model_fname[:-3] + "_scaler.bin"

    data = np.load(args.filename)
    print("Starting...")
    apply(
        data, args.model_fname, args.scaler_fname, args.out_fname
    )
    print(f"Output saved in {args.out_fname}.root")
