import os
import ROOT
import ctypes

def _remove_canvas_if_exists(name):
    can = ROOT.gROOT.GetListOfCanvases().FindObject(name)
    if isinstance(can, ROOT.TCanvas):
        can.Destructor()


def _remove_th1_if_exists(name):
    th1 = ROOT.gROOT.FindObject(name)
    if isinstance(th1, ROOT.TH1D):
        th1.Delete()


def _scale_frame(frame, factor=.75):
    """Takes RooPlot object and applies some label scaling to it.

    Args:
        factor: scaling factor [float] (default: 0.75)

    Returns:
        frame: Scaled RooPlot object"""
    frame.GetXaxis().SetTitleSize(0.06 * factor)
    frame.GetXaxis().SetLabelSize(0.06 * factor)
    frame.GetXaxis().SetTickSize(0.05)

    frame.GetYaxis().SetTitleSize(0.072 * factor)
    frame.GetYaxis().SetTitleOffset(0.95 / factor)
    frame.GetYaxis().SetLabelSize(0.06 * factor)

    return frame


def plot_simple(name, frame, save_path=None,
                drawables=None, logx=False, logy=False, yrange=None, appendices=['.pdf']):
    """Takes a RooFrame object and plots it on a TCanvas.

    Args:
        name: Name of the plot [string]
        frame: Frame object [RooPlot]
        save_path: If path is given, the TCanvas will be saved at this location
                   [string] (default: None)
        drawables: Add drawables (i.e. classes with `.Draw()` method) in absolute scale,
                   e.g. TLine(5200, 0, 5400, 1000). If you want to use relative scale,
                   call 'object.SetNDC()'. Example for relative scale:
                   > latex = TLatex(0.5, 0.75, 'LHCb Unofficial')
                   > latex.SetNDC()
        logx: Logarithms x-axis [boolean] (default: False)
        logy: Logarithms y-axis [boolean] (default: False)
        yrange: Optionally set a range for the y range
        appendix: Plot appendix which defines file type [list of string]
                  (default: '.pdf')

    Returns:
        can: Drawn TCanvas
        (lines, pulls): List of drawables (only needed
                        for visualization in notebooks)

    Example usage:
        >>> # The following command creates a pull plot and saves it to
        >>> # '/home/jdoe/plots/B_0_mass.pdf'
        >>> can, _ = plot_pulls('B_0_mass', plot_frame, logy=True,
        >>>                     save_path='/home/jdoe/plots')
        >>> can
        >>> # If rootnotes is imported, this will display the plot in
        >>> the notebook
    """
    # To simplify usage
    import collections
    if drawables:
        if not isinstance(drawables, collections.MutableSequence):
            drawables = [drawables]
    else:
        drawables = []

    if logy:
        frame.SetMaximum(frame.GetMaximum() * 1.5)

    # Delete previously defined canvas, if exists, to avoid this warning:
    # TCanvas::Constructor:0: RuntimeWarning: Deleting canvas with same name: can
    _remove_canvas_if_exists('can')
    can = ROOT.TCanvas('can', 'can', 800, 600)
    ROOT.SetOwnership(can, False)

    frame = _scale_frame(frame)
    if yrange is not None:
        frame.SetMaximum(yrange[1])
        frame.SetMinimum(yrange[0])

    # frame.SetMinimum(0.5)
    frame.Draw()

    if logx:
        can.SetLogx()
    if logy:
        can.SetLogy()

    # Add drawables like TLines, TLatex...
    for dr in drawables:
        dr.Draw()

    can.Draw()

    if save_path:
        for appendix in appendices:
            can.SaveAs(os.path.join(save_path, name) + appendix)

    return can, [drawables]


def plot_pulls(name, frame, save_path=None,
               drawables=None, logx=False, logy=False, yrange=None, spacing=False,
               ignore_range_in_pulls=None, appendices=['.pdf'], pullstyle='default', danger_pulls=None, pullcolor=None):
    """Takes a RooFrame object and plots it on a TCanvas with pulls underneath.

    Args:
        name: Name of the plot [string]
        frame: Frame object [RooPlot]
        save_path: If path is given, the TCanvas will be saved at this location
                   [string] (default: None)
        drawables: Add drawables (i.e. classes with `.Draw()` method) in absolute scale,
                   e.g. TLine(5200, 0, 5400, 1000). If you want to use relative scale,
                   call 'object.SetNDC()'. Example for relative scale:
                   > latex = TLatex(0.5, 0.75, 'LHCb Unofficial')
                   > latex.SetNDC()
        logx: Logarithms x-axis [boolean] (default: False)
        logy: Logarithms y-axis [boolean] (default: False)
        yrange: Overrides y-axis range [list type with length 2].
                Useful when logy == True. (default: None)
        spacing: Insert a spacing between main frame and pull frame
        ignore_range_in_pulls: Ignores certain range in pulls, inclusive
                               [list type with length 2, e.g.: (100, 110)] (default: None)
        appendix: Plot appendix which defines file type [list of string]
                  (default: '.pdf')
        pullstyle: Style for pulls. Possible values:
                    * default (Standard: draw thick bars)
                    * flat (No vertical bars)
                    * hist (Outline histogram, no vertical bars)
                    * data (data points)
        dangerpulls: Whether to color danger pulls in red, default = True
        pullcolor: Whether to color danger pulls in red, default = kGray
    Returns:
        can: Drawn TCanvas
        (lines, pulls): List of lines and RooPlot object of pulls (only needed
                        for visualization in notebooks)

    Example usage:
        >>> # The following command creates a pull plot and saves it to
        >>> # '/home/jdoe/plots/B_0_mass.pdf'
        >>> can, _ = plot_pulls('B_0_mass', plot_frame, logy=True,
        >>>                     save_path='/home/jdoe/plots')
        >>> can
        >>> # If rootnotes is imported, this will display the plot in
        >>> the notebook
    """
    # To simplify usage
    import collections
    if drawables:
        if not isinstance(drawables, collections.MutableSequence):
            drawables = [drawables]
    else:
        drawables = []

    # Delete previously defined canvas, if exists, to avoid this warning:
    # TCanvas::Constructor:0: RuntimeWarning: Deleting canvas with same name: can
    _remove_canvas_if_exists('can')

    can = ROOT.TCanvas('can', 'can', 800, 600)
    ROOT.SetOwnership(can, False)

    main_frame_height = 0.75
    pull_frame_height = 0.25
    main_pull_ratio = main_frame_height / pull_frame_height
    num_sigma_pull_range = 5.99

    # Create RooPlot object for pulls
    frame_pulls = ROOT.RooPlot(frame.GetXaxis().GetXmin(),
                               frame.GetXaxis().GetXmax())
    frame_pulls.GetXaxis().SetTitle(frame.GetXaxis().GetTitle())
    ROOT.SetOwnership(frame_pulls, False)

    # Pad 1
    pad1 = ROOT.TPad("pad1", "The main pad", 0.0, 1 - main_frame_height, 1.0, 1.0)

    # Insert a spacing between main frame and pull frame, if spacing is set to True
    if spacing:
        pad1.SetBottomMargin(0.05)
    else:
        pad1.SetBottomMargin(0)

    ROOT.SetOwnership(pad1, False)

    pad1.cd()

    # Manipulate and draw pad1
    # frame.SetMinimum(0.5)
    if yrange is not None:
        if len(yrange) == 2:
            if yrange[1] < yrange[0]:
                print('ERROR: Upper yrange[0] must be < yrange[1]!')
                return (0, 0)
            if yrange[0] <= 0.0 and logy:
                print('ERROR: Lower yrange limit must be > 0')
                return (0, 0)
        else:
            print('ERROR: yrange must be a two component list!')
            return (0, 0)
    if logy:
        if yrange is not None:
            frame.SetMaximum(yrange[1])
            frame.SetMinimum(yrange[0])
        else:  # default case
            frame.SetMaximum(frame.GetMaximum() * 1.5)
    if yrange is not None:
        frame.SetMaximum(yrange[1])
        frame.SetMinimum(yrange[0])

    frame.GetXaxis().SetLabelSize(0.0)
    # Remove the x-axis title of the main frame
    frame.GetXaxis().SetTitleSize(0.0)

    frame.Draw()

    # Pad 2
    pad2 = ROOT.TPad("pad2", "The pull pad", 0.0, 0.0, 1.0, pull_frame_height)
    pad2.SetTopMargin(0)
    pad2.SetBottomMargin(0.4)  # The magic happens here!
    pad2.cd()
    ROOT.SetOwnership(pad2, False)

    # Manipulate and draw pad2
    hpull = frame.pullHist()
    ROOT.SetOwnership(hpull, False)

    if hpull is None:
        print('ERROR: No RooCurve found! Will skip plotting...')
        return (0, 0)

    bins = frame.GetNbinsX()
    range_min_x = frame.GetXaxis().GetXmin()
    range_max_x = frame.GetXaxis().GetXmax()

    # Create new TH1 objects for pulls
    if pullcolor is None:
        ok_color = ROOT.kGray if pullstyle == "default" else ROOT.kBlue
    else:
        ok_color = pullcolor

    if danger_pulls is None:
        danger_color = ROOT.kRed
    else:
        danger_color = ok_color
    danger_limit = 3  # Greater than +- 3 Sigma

    # Delete previously defined TH1D, if exists, to avoid this warning:
    # TROOT::Append:0: RuntimeWarning: Replacing existing TH1: {name} (Potential memory leak).
    for th1_name in ['pulls_all', 'pulls_ok', 'pulls_danger']:
        _remove_th1_if_exists(th1_name)

    pulls_all = ROOT.TH1D('pulls_all', 'pulls_all', bins, range_min_x, range_max_x)
    pulls_ok = ROOT.TH1D('pulls_ok', 'pulls_ok', bins, range_min_x, range_max_x)
    pulls_danger = ROOT.TH1D('pulls_danger', 'pulls_danger', bins, range_min_x, range_max_x)
    ROOT.SetOwnership(pulls_all, False)
    ROOT.SetOwnership(pulls_ok, False)
    ROOT.SetOwnership(pulls_danger, False)

    pulls_all.SetFillColor(ok_color)
    pulls_danger.SetFillColor(danger_color)

    if pullstyle == "default":
        pulls_all.SetFillStyle(0)
    if pullstyle == "flat":
        pulls_all.SetLineWidth(0)
        pulls_ok.SetLineWidth(0)
        pulls_danger.SetLineWidth(0)
    if pullstyle == "hist":
        pulls_all.SetLineWidth(1)
        pulls_ok.SetLineWidth(0)
        pulls_danger.SetLineWidth(0)
    if pullstyle == "data":
        pass

    n_danger_bins = 0
    for i in range(1, bins+1):
        cur_x, cur_y = ctypes.c_double(0.), ctypes.c_double(0.)
        hpull.GetPoint(i-1, cur_x, cur_y)  # Gets values per reference
        cur_x, cur_y = cur_x.value, cur_y.value
        if ignore_range_in_pulls and ignore_range_in_pulls[0] <= cur_x <= ignore_range_in_pulls[1]:
            pulls_all.SetBinContent(i, 0)
        else:
            # Needed for plotting pulls_danger
            if abs(cur_y) > num_sigma_pull_range and abs(cur_y) == cur_y:
                cur_y = num_sigma_pull_range
            elif abs(cur_y) > num_sigma_pull_range and abs(cur_y) != cur_y:
                cur_y = -num_sigma_pull_range

            pulls_all.SetBinContent(i, cur_y)
            pulls_ok.SetBinContent(i, 0)
            pulls_danger.SetBinContent(i, 0)

            if abs(cur_y) > danger_limit:
                pulls_danger.SetBinContent(i, cur_y)
                n_danger_bins += 1
            else:
                pulls_ok.SetBinContent(i, cur_y)

    # Configure frame
    frame_pulls.SetMinimum(-num_sigma_pull_range)
    frame_pulls.SetMaximum(num_sigma_pull_range)

    frame_pulls = _scale_frame(frame_pulls, main_pull_ratio)
    frame_pulls.GetYaxis().SetTitle('Pull')
    frame_pulls.Draw()

    # Add sigma lines
    zeroline = ROOT.TLine(range_min_x, 0.0, range_max_x, 0.0)
    ROOT.SetOwnership(zeroline, False)
    if pullstyle == "flat":
        zeroline.SetLineWidth(0)

    threesigmaline = ROOT.TLine(range_min_x, 3.0, range_max_x, 3.0)
    threesigmaline.SetLineStyle(ROOT.kDashed)
    threesigmaline.SetLineColor(ROOT.kGray + 2)
    ROOT.SetOwnership(threesigmaline, False)

    minusthreesigmaline = ROOT.TLine(range_min_x, -3.0, range_max_x, -3.0)
    minusthreesigmaline.SetLineStyle(ROOT.kDashed)
    minusthreesigmaline.SetLineColor(ROOT.kGray + 2)
    ROOT.SetOwnership(minusthreesigmaline, False)

    # Draw all the stuff
    zeroline.Draw('SAME')
    threesigmaline.Draw('SAME')
    minusthreesigmaline.Draw('SAME')

    if pullstyle == "default":
        pulls_all.Draw('SAME')
        pulls_danger.Draw('SAME')
        pulls_ok.Draw('SAME')
        pulls_all.Draw('SAME BAR')
    elif pullstyle == "flat":
        pulls_all.Draw('SAME')
        if danger_pulls is True:
            pulls_danger.Draw('SAME')
    elif pullstyle == "hist":
        pulls_all.Draw('SAME')
        if danger_pulls is True:
            pulls_danger.Draw('SAME')
    elif pullstyle == "data":
        pulls_all.Draw('SAME E0')

    # Add drawables like TLines, TLatex...
    pad1.cd()
    for dr in drawables:
        dr.Draw()

    # Canvas
    can.cd()

    if logx:
        pad1.SetLogx()
        pad2.SetLogx()
        # Due to the ticks label exponents, one needs to adjust the
        # offset between the x axis title and x axis ticks.
        frame_pulls.GetXaxis().SetLabelOffset(-0.04)
    if logy:
        pad1.SetLogy()

    pad1.Draw()
    pad2.Draw('SAME')

    can.Draw()

    if save_path:
        for appendix in appendices:
            can.SaveAs(os.path.join(save_path, name) + appendix)

    return can, [zeroline, threesigmaline, minusthreesigmaline, frame_pulls,
                 pulls_all, pulls_ok, pulls_danger, drawables], (n_danger_bins / bins)


def get_fitresult_parameters(fitres, do_print=False,
                             formatter="{:<20}{:<8.3e} +/- {:<8.3e}\t[{:<5.2e} - {:<5.2e}]\tConst: {}"):
    """Takes (and prints) all values and errors of RooRealVars stored in RooFitResults.
    """
    from collections import namedtuple
    Var = namedtuple('Var', ['Name', 'Val', 'Err',
                             'Min', 'Max', 'IsConst'])

    vars_list = []

    for i in range(fitres.floatParsFinal().size()):
        cur_var = fitres.floatParsFinal()[i]

        cur_name = cur_var.GetName()
        cur_val = cur_var.getVal()
        cur_err = cur_var.getError()
        cur_min = cur_var.getMin()
        cur_max = cur_var.getMax()
        cur_const = cur_var.isConstant()

        var_to_add = Var(cur_name, cur_val, cur_err,
                         cur_min, cur_max, cur_const)

        vars_list.append(var_to_add)
        if do_print:
            print(formatter.format(*var_to_add))
    return vars_list
