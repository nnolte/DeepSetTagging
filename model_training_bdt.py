#!/usr/bin/env python
# coding: utf-8

import argparse
import numpy as np
import lightgbm as lgb
from sklearn.metrics import roc_auc_score
from IPython import embed
from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.model_selection import train_test_split
from sklearn.linear_model import SGDClassifier
from copy import copy
import matplotlib.pyplot as plt

def logit(x):
  return np.log(x/(1-x))

def expit(x):
  return 1 / (1 + np.exp(-x))


def write_epm_file(preds, truth, epm_fname):
    import uproot3

    pred_tags = preds.squeeze()

    epm_tags = np.where(pred_tags > 0.5, 1, -1).astype(np.int32)
    true_id = np.where(truth.squeeze() == 0, -511, 511).astype(np.int32)
    eta = np.where(pred_tags > 0.5, 1 - pred_tags, pred_tags)

    with uproot3.recreate(f"{epm_fname}.root", compression=None) as file:
        file["DecayTree"] = uproot3.newtree({"B_TRUEID": np.int32, "tag": np.int32, "eta": np.float64})
        t = file["DecayTree"]
        t["B_TRUEID"].newbasket(true_id)
        t["tag"].newbasket(epm_tags)
        t["eta"].newbasket(eta)


def train_model(files, model_out_name, n_epochs, train_frac, batch_size, make_epm_output):

    n_evts_total = 100000000000000

    print("Starting Training")

    features = np.concatenate([f["features"] for f in files])



    evt_borders = files[0]["evt_borders"][:n_evts_total +1]
    features = features[:evt_borders[-1]]

    # probnnmu has a bin at -1, for particles that don't have muon info
    # map that to 0
    features[features[:, 3] == -1, 3] = 0

    # take out the charge
    charge = features[:,8]
    features = np.delete(features, 8, 1)


    for f in files[1:]:
        evt_borders = np.concatenate((evt_borders, f["evt_borders"][1:] + evt_borders[-1]))

    assert evt_borders[-1] == len(features)

    evt_tags = np.concatenate([f["B_TRUEID"] for f in files])[:n_evts_total]
    evt_tags = np.where(evt_tags == 511, 1, -1).astype(np.int32)
    tags = np.repeat(evt_tags, evt_borders[1:] - evt_borders[:-1])
    tags = ((tags * charge) > 0).astype(np.int32)

    evt_split = int(len(evt_borders) * train_frac)
    split = evt_borders[evt_split]

    train_tags = tags[:split]
    train_feat = features[:split]
    train_charge = charge[split:]

    test_tags = tags[split:]
    test_feat = features[split:]
    test_charge = charge[split:]

    test_evt_borders = evt_borders[evt_split:] - evt_borders[evt_split]


    model = lgb.LGBMClassifier(max_depth=5, num_leaves=10, learning_rate=.2, n_estimators=n_epochs)

    model.fit(train_feat, train_tags, eval_set=[(test_feat, test_tags)], early_stopping_rounds=20, verbose=True)


    test_pred = model.predict_proba(test_feat)[:,1]

    test_pred_cal, _ = calibrate_probs(test_tags, np.ones(len(test_tags)), test_pred)

    print(roc_auc_score(test_tags, test_pred))
    print(roc_auc_score(test_tags, test_pred_cal))

    
    #Yandex implementation
    sign_b = np.repeat(evt_tags[evt_split:], test_evt_borders[1:] - test_evt_borders[:-1])
    N_B_events = len(test_evt_borders) - 1
    event_ids = np.repeat(list(range(len(test_evt_borders) - 1)), test_evt_borders[1:] - test_evt_borders[:-1])
    Bsign, Bweight, B_probs, Bevent, auc_full = get_B_data_for_given_part(test_tags, test_pred_cal, event_ids, test_charge, sign_b, N_B_events)
    
    # our implementation
    # for each event, multiply track (pred/(1-pred))^charge
    test_preds_evt = [test_pred_cal[l:h] for l,h in zip(test_evt_borders[:-1], test_evt_borders[1:])]
    test_charges_evt = [test_charge[l:h] for l,h in zip(test_evt_borders[:-1], test_evt_borders[1:])]
    B_probs = np.array([np.multiply.reduce((tp/(1-tp))**tc) for tp,tc in zip(test_preds_evt, test_charges_evt)])
    B_probs[np.isnan(B_probs)] = .5
    B_probs[np.abs(B_probs) == np.inf] = .5
    B_probs = B_probs / (B_probs + 1)

    print(roc_auc_score(evt_tags[evt_split:], B_probs))









    
    # if make_epm_output:
    #     print("Writing output for EPM")
    #     try:
    #         write_epm_file(mypreds, test_tags_np, f"{model_out_name}_epm")
    #     except ImportError:
    #         print("Option make-epm-output requires uproot3 package to be available.\n Writing of EPM output skipped!")

    # print("Making plots.")
    # import matplotlib
    # import matplotlib.pyplot as plt

    # matplotlib.rcParams.update({"font.size": 22})

    # plt.figure(figsize=(16, 9))
    # plt.plot(all_tl, label="Train Loss")
    # plt.plot(all_vl, label="Validation Loss")
    # plt.legend()
    # plt.xlabel("Epoch")
    # plt.ylim(0.6, 0.8)
    # plt.grid()
    # plt.savefig("Loss_vs_Epoch.png")


def calibrate_probs(labels, weights, probs, random_state=42, threshold=0, symmetrize=False):
    """
    Calibrate output to probabilities using 2-folding to calibrate all data
    
    :param probs: probabilities, np.array of shape [n_samples]
    :param labels: np.array of shape [n_samples] with labels 
    :param weights: np.array of shape [n_samples]
    :param threshold: float, to set labels 0/1 
    :param logistic: bool, use logistic or isotonic regression
    :param symmetrize: bool, do symmetric calibration, ex. for B+, B-
    
    :return: calibrated probabilities, calibration class
    """
    calibration = CalibrationProcedure(random_state=random_state, threshold=threshold, symmetrize=symmetrize)
    calibration.fit(probs, labels, sample_weight=weights)
    probs_calibrated = calibration.predict_proba(probs)
    return probs_calibrated, calibration


class CalibrationProcedure(BaseEstimator, ClassifierMixin):
    """
    Calibration class: implements 2-folds calibration with calibration procedures
    * logistic regression 
    * isotonic regression
    """
    def __init__(self, symmetrize=False, random_state=42, threshold=0.):
        """
        :param bool logistic: logistic regression (for True) or isotonic regression (for False) will be used
        :param bool symmetrize: symmetrize samples or not. If True then add inverse predictions with inverse labels
        :param float threshold: threshold for lables: y > threshold is 1 class and y <= threshold is 0 class.
        """
        self.symmetrize = symmetrize
        self.random_state = random_state
        self.threshold = threshold
    
    def _compute_inds(self, length):
        ind = np.arange(length)
        ind_1, ind_2 = train_test_split(ind, random_state=self.random_state, train_size=0.5)
        return ind_1, ind_2
            
    def fit(self, X, y, sample_weight=None):
        """
        Train calibration rule for input probabilities `X` with target values `y`
        """
        labels = (y > self.threshold) * 1
        probs = X
        weights = np.ones(len(probs)) if sample_weight is None else sample_weight
        ind_1, ind_2 = self._compute_inds(len(probs))

        calibrator = SGDClassifier(loss='log', random_state=self.random_state, penalty='none', alpha=0.01)
        est_calib_1, est_calib_2 = copy(calibrator), copy(calibrator)
        probs_1 = probs[ind_1]
        probs_2 = probs[ind_2]

        probs_1 = np.clip(probs_1, 0.001, 0.999)
        probs_2 = np.clip(probs_2, 0.001, 0.999)
        probs_1 = probs_1[:, np.newaxis]
        probs_2 = probs_2[:, np.newaxis]
        if self.symmetrize:
            est_calib_1.fit(np.r_[logit(probs_1), logit(1-probs_1)], 
                            np.r_[labels[ind_1] > 0, labels[ind_1] <= 0],
                            sample_weight=np.r_[weights[ind_1], weights[ind_1]])
            est_calib_2.fit(np.r_[logit(probs_2), logit(1-probs_2)], 
                            np.r_[labels[ind_2] > 0, labels[ind_2] <= 0],
                            sample_weight=np.r_[weights[ind_2], weights[ind_2]])
        else:
            est_calib_1.fit(logit(probs_1), labels[ind_1], sample_weight=weights[ind_1])
            est_calib_2.fit(logit(probs_2), labels[ind_2], sample_weight=weights[ind_2])
        self.calibrators = [est_calib_1, est_calib_2]
        return self

    def predict_proba(self, X):
        """
        Predict calibrated probabilities for input probabilities `X`
        """
        probs = X
        calibrated_probs = np.zeros(len(probs))
        ind_1, ind_2 = self._compute_inds(len(probs))

        probs_1 = probs[ind_1]
        probs_2 = probs[ind_2]
        
        probs_1 = np.clip(probs_1, 0.001, 0.999)
        probs_2 = np.clip(probs_2, 0.001, 0.999)
        probs_1 = probs_1[:, np.newaxis]
        probs_2 = probs_2[:, np.newaxis]
        calibrated_probs[ind_1] = self.calibrators[1].predict_proba(logit(probs_1))[:, 1]
        calibrated_probs[ind_2] = self.calibrators[0].predict_proba(logit(probs_2))[:, 1]
        
        return calibrated_probs



def compute_B_prob_using_part_prob(event_ids, sign_part, sign_b, probs):
    
    result_event_id, data_ids = np.unique(event_ids, return_inverse=True)
    log_probs = np.log(probs) - np.log(1 - probs)
    sign_weights = np.ones(len(log_probs))
    log_probs *= sign_weights * sign_part
    result_logprob = np.bincount(data_ids, weights=log_probs)
    # simply reconstructing original
    result_label = np.bincount(data_ids, weights=sign_b) / np.bincount(data_ids)
    result_weight = np.bincount(data_ids) / np.bincount(data_ids)
    return result_label, result_weight, expit(result_logprob), result_event_id


def get_B_data_for_given_part(label, part_probs, event_ids, sign_part, sign_b, N_B_events,
                              part_name='track',
                              random_state=42):
    
    # Calibration p(track/vertex same sign|B)
    part_probs_calib, _ = calibrate_probs(label, np.ones(len(label)), part_probs, 
                                                    random_state=random_state)
    # plt.figure(figsize=[18, 5])
    # plt.subplot(1,3,1)
    # plt.hist(part_probs[label == 0], bins=60, density=True, alpha=0.3, label='os')
    # plt.hist(part_probs[label == 1], bins=60, density=True, alpha=0.3, label='ss')
    # plt.legend(), plt.title('{} probs'.format(part_name))
    
    # plt.subplot(1,3,2)
    # plt.hist(part_probs_calib[label == 0], bins=60, density=True, alpha=0.3, label='os')
    # plt.hist(part_probs_calib[label == 1], bins=60, density=True, alpha=0.3, label='ss')
    # plt.legend(), plt.title('{} probs calibrated'.format(part_name))
        
    
    # Compute p(B+)
    Bsign, Bweight, Bprob, Bevent = compute_B_prob_using_part_prob(event_ids, sign_part, sign_b, part_probs_calib)
    Bprob[~np.isfinite(Bprob)] = 0.5
    Bprob[np.isnan(Bprob)] = 0.5
    
    # plt.subplot(1,3,3)
    # plt.hist(Bprob[np.array(Bsign) == -1], bins=60, density=True, alpha=0.3, label='$B^-$')
    # plt.hist(Bprob[np.array(Bsign) == 1], bins=60, density=True, alpha=0.3, label='$B^+$')
    # plt.legend(), plt.title('B probs'), plt.show()
    
    auc, auc_full = calculate_auc_with_and_without_untag_events(Bsign, Bprob, Bweight, N_B_events=N_B_events)
    print('AUC for tagged:', auc, 'AUC with untag:', auc_full)
    return Bsign, Bweight, Bprob, Bevent, auc_full


def calculate_auc_with_and_without_untag_events(Bsign, Bprobs, Bweights, N_B_events):
    """
    Calculate AUC score for data and AUC full score for data and untag data (p(B+) for untag data is set to 0.5)
    
    :param Bprobs: p(B+) probabilities, numpy.array of shape [n_samples]
    :param Bsign: numpy.array of shape [n_samples] with labels {-1, 1}
    :param Bweights: numpy.array of shape [n_samples]
    
    :return: auc, full auc
    """
    N_B_not_passed = N_B_events - sum(Bweights)
    Bsign_not_passed = [-1, 1]
    Bprobs_not_passed = [0.5] * 2
    Bweights_not_passed = [N_B_not_passed / 2.] * 2
    
    def union(*x):
      return np.concatenate(x)

    auc_full = roc_auc_score(union(Bsign, Bsign_not_passed), union(Bprobs, Bprobs_not_passed),
                             sample_weight=union(Bweights, Bweights_not_passed))
    auc = roc_auc_score(Bsign, Bprobs, sample_weight=Bweights)
    return auc, auc_full


def restricted_float(x):
    try:
        x = float(x)
    except ValueError:
        raise argparse.ArgumentTypeError(f"{x} not a floating-point literal")

    if x <= 0.0 or x > 1.0:
        raise argparse.ArgumentTypeError(f"{x} not in range (0.0, 1.0]")

    return x


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Train Model for Flavour Tagging.")
    parser.add_argument("filenames", nargs="+", help="Files that contain training data. *.npz files expected)")
    parser.add_argument(
        "-model-out-name",
        default="model_bdt",
        help="File name to save weights into. Default is model.pt",
    )
    parser.add_argument("-epochs", dest="n_epochs", default=1000, type=int, help="Batch size")
    parser.add_argument(
        "-train-frac",
        default=0.75,
        type=restricted_float,
        help="Fraction of data to use for training",
    )
    parser.add_argument("-batch-size", default=1000, type=int, help="Batch size")
    parser.add_argument("--make-epm-output", action="store_false", help="Write tagged validataion data into root file for EPM")

    args = parser.parse_args()

    files = [np.load(f) for f in args.filenames]

    train_model(
        files, args.model_out_name, args.n_epochs, args.train_frac, args.batch_size, args.make_epm_output
    )
