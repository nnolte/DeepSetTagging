#!/usr/bin/env python3

import uproot
import numpy as np
import os
import argparse
from feature_names import features

def preprocess(tree, cut, extra_evt, extra_trk):

    data_features = ["B_IFT_Bd_InclusiveTagger_TagPartsFeature_NUM"] + features + extra_evt + extra_trk
    data = tree.arrays(data_features, cut=cut, library="np")
    # PROBLEM: differnt number of Tr_ORIG_FLAGS and features, so not sure how to proceed
    # number of tracks per event
    evt_sizes = data["B_IFT_Bd_InclusiveTagger_TagPartsFeature_NUM"]
    evt_idx = np.cumsum(np.concatenate(([0], evt_sizes)), dtype=np.int32)
    ntracks_total = sum(evt_sizes)
    borders = np.array(list(zip(evt_idx[:-1], evt_idx[1:])))

    assert len(features) == 18
    out_data = np.zeros((ntracks_total, 18), dtype=np.float32)

    for idx, name in enumerate(features):
        out_data[:, idx] = np.concatenate(data[name])

    assert out_data.strides == (72, 4)
    # layout should be 18 features of Track 0| Track 1 | Track 2
    # with features continous in memory, so strides should be
    # (72,4) : 72

    # vector that maps each track to it original event
    # needed to later recalculate borders after filtering
    # and remove empty events
    tr_2_evt_idx = np.zeros(ntracks_total, dtype=np.int64)
    for i, (b, e) in enumerate(borders):
        tr_2_evt_idx[b:e] = i

    # tuple tool tagging seems to flag some tracks as "broken" by filling this fake value for all features
    mask = out_data[:, 0] != -99999
    print(f"Filtering out {np.sum(~mask)} tracks")
    out_data = out_data[mask]
    # which events still have # tracks > 0 and how many tracks does each event have?
    kept_evts, counts = np.unique(tr_2_evt_idx[mask], return_counts=True)
    # just the borders of each event
    evt_borders = np.cumsum(np.concatenate(([0], counts)))

    # throw away the events we filtered above
    # TODO right now extra can only contain per Event quantities
    # needs more logic to work for per track ones
    extra_out = dict()
    for e in extra_evt:
        extra_out[e] = data[e][kept_evts]
    for e in extra_trk:
        extra_out[e] = data[e][mask]

    print(f"Selection eff of passed cut: {len(evt_sizes) / tree.num_entries}")
    print(f"Filtered out {len(evt_sizes) - len(kept_evts)} / {len(evt_sizes)} Events, after track filtering ")

    return {
        # 1 Entry per Track:
        "features": out_data,
        # 1 Entry per Event:
        "evt_borders": evt_borders,
        **extra_out,
    }


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Preprocess (MC-)data for Flavour Tagging.")
    parser.add_argument("filename", help="File to be processed. (ROOT,(L)DST file expected)")
    parser.add_argument("-t", dest="tree_name", default="DecayTree", help="Full path of TTree to process. Default: 'DecayTree'")
    parser.add_argument("-o", dest="out_name", default=None, help="specify name for output file. Default is INPUT_FILENAME.npz")
    parser.add_argument("--is-mc", default=False, action="store_true", help="specify if the input is mc data")
    args = parser.parse_args()

    try:
        tree = uproot.open(args.filename, timeout=60)[args.tree_name]
    except FileNotFoundError:
        print(f"Could not find file: '{args.filename}'. Exiting.")
        exit(1)
    except uproot.exceptions.KeyInFileError:
        print(f"Could not find any TTree at: '{args.tree_name}'")
        exit(1)

    # cut = "B_L0Global_Dec\
    #        & (Jpsi_Hlt1DiMuonHighMassDecision_TOS | B_Hlt1TrackMuonDecision_TOS | Jpsi_Hlt1TwoTrackMVADecision_TOS)\
    #        & B_Hlt2DiMuonDetachedJPsiDecision_TOS\
    #        & (piminus_PT > 250)\
    #        & ( (B_LOKI_DTF_CTAU / 0.29979245) > 0.3)\
    #        & ( (B_LOKI_DTF_CTAU / 0.29979245) < 15.0)"

    print(f"Starting processing of {args.filename}")
    if args.is_mc:
        # cut += "& ( ( (B_BKGCAT == 50) & (B_LOKI_DTF_CTAU  > 0) ) | (B_BKGCAT == 0) | (B_BKGCAT == 10) )"
        extra_evt = ["B_TRUEID"]
        extra_trk = []
        out = preprocess(tree, None, extra_evt, extra_trk)
    else:
        extra = ["B_TAU", "B_TAUERR", "B_M", "B_ID"]
        out = preprocess(tree, None, extra, [])

    if args.out_name == None:
        args.out_name = os.path.basename(args.filename)
        args.out_name = args.out_name.split(".")[0]

    np.savez(args.out_name, **out)

    print(f"Processed data stored in {args.out_name}.npz")
