features = [
    "B_IFT_Bd_InclusiveTagger_TagPartsFeature_PROBNNe",
    "B_IFT_Bd_InclusiveTagger_TagPartsFeature_PROBNNghost",
    "B_IFT_Bd_InclusiveTagger_TagPartsFeature_PROBNNk",
    "B_IFT_Bd_InclusiveTagger_TagPartsFeature_PROBNNmu",
    "B_IFT_Bd_InclusiveTagger_TagPartsFeature_PROBNNp",
    "B_IFT_Bd_InclusiveTagger_TagPartsFeature_PROBNNpi",
    "B_IFT_Bd_InclusiveTagger_TagPartsFeature_P",
    "B_IFT_Bd_InclusiveTagger_TagPartsFeature_PT",
    "B_IFT_Bd_InclusiveTagger_TagPartsFeature_Q",
    "B_IFT_Bd_InclusiveTagger_TagPartsFeature_BPVIP",
    "B_IFT_Bd_InclusiveTagger_TagPartsFeature_BPVIPCHI2",
    "B_IFT_Bd_InclusiveTagger_TagPartsFeature_SumBDT_ult",
    "B_IFT_Bd_InclusiveTagger_TagPartsFeature_PP_VeloCharge",
    "B_IFT_Bd_InclusiveTagger_TagPartsFeature_IP_Mother",
    "B_IFT_Bd_InclusiveTagger_TagPartsFeature_diff_z",
    "B_IFT_Bd_InclusiveTagger_TagPartsFeature_P_proj",
    "B_IFT_Bd_InclusiveTagger_TagPartsFeature_cos_diff_phi",
    "B_IFT_Bd_InclusiveTagger_TagPartsFeature_diff_eta",
]
