#include "json.hpp"
#include "prettyprint.h"
#include "simplenn/Dense.h"
#include <array>
#include <chrono>
#include <fstream>
#include <iostream>
#include <random>

using namespace simpleNN;

template <int n_in, int n_out, typename procs_out = procs::id>
constexpr auto makeDenseLayer(std::vector<float> const &weights,
                              std::vector<float> const &biases) {
  return Dense{std::array<procs_out, n_out>{}, std::array<procs::id, n_in>{},
               biases, weights};
}

auto readJSON(std::string file_loc) {
  using json = nlohmann::json;
  std::ifstream f{file_loc};
  std::string file_content{std::istreambuf_iterator<char>{f}, {}};
  return json::parse(file_content);
}

template <int nfeatures> auto generate_random(int n) {
  std::random_device rd{};
  std::default_random_engine eng{rd()};
  std::uniform_real_distribution<float> distr(-5, 5);

  std::vector<std::array<float, nfeatures>> out{};
  out.reserve(n);
  auto gen_arr = [&] {
    std::array<float, nfeatures> x{};
    std::generate_n(x.begin(), nfeatures, [&] { return distr(eng); });
    return x;
  };
  std::generate_n(std::back_inserter(out), n, gen_arr);
  return out;
}

template <int nfeatures> auto generate_events(int n) {
  std::random_device rd{};
  std::default_random_engine eng{rd()};
  std::uniform_int_distribution<int> distr(1, 100);

  std::vector<std::vector<std::array<float, nfeatures>>> events;
  events.reserve(n);
  std::generate_n(std::back_inserter(events), n,
                  [&] { return generate_random<nfeatures>(distr(eng)); });
  return events;
}

constexpr static std::size_t in_feature_dim = 18;
constexpr static std::size_t lat_space_dim = 18;
constexpr static std::size_t summed_out_dim = 20;
constexpr static std::size_t final_out_dim = 1;

using l1_t =
    simpleNN::Dense<std::array<simpleNN::procs::ReLU, in_feature_dim>,
                    std::array<simpleNN::procs::linear<double>, in_feature_dim>,
                    double>;
using l2_t =
    simpleNN::Dense<std::array<simpleNN::procs::ReLU, lat_space_dim>,
                    std::array<simpleNN::procs::id, in_feature_dim>, double>;
using l3_t =
    simpleNN::Dense<std::array<simpleNN::procs::ReLU, summed_out_dim>,
                    std::array<simpleNN::procs::id, lat_space_dim>, double>;
using l4_t =
    simpleNN::Dense<std::array<simpleNN::procs::sigmoid, final_out_dim>,
                    std::array<simpleNN::procs::id, summed_out_dim>, double>;

int main() {
  auto params = readJSON("../output/model.json");

  auto weights_l1 = params["weight1"].get<std::vector<double>>();
  auto weights_l2 = params["weight2"].get<std::vector<double>>();
  auto weights_l3 = params["weight3"].get<std::vector<double>>();
  auto weights_l4 = params["weight4"].get<std::vector<double>>();
  auto biases_l1 = params["bias1"].get<std::vector<double>>();
  auto biases_l2 = params["bias2"].get<std::vector<double>>();
  auto biases_l3 = params["bias3"].get<std::vector<double>>();
  auto biases_l4 = params["bias4"].get<std::vector<double>>();
  auto scales = params["scales"].get<std::vector<double>>();
  auto offsets = params["offsets"].get<std::vector<double>>();

  auto l1 = l1_t{std::array<simpleNN::procs::ReLU, 18>{},
                 std::array<simpleNN::procs::linear<double>, 18>{
                     simpleNN::procs::linear<double>(1.0 / scales[0],
                                                     -offsets[0] / scales[0]),
                     simpleNN::procs::linear<double>(1.0 / scales[1],
                                                     -offsets[1] / scales[1]),
                     simpleNN::procs::linear<double>(1.0 / scales[2],
                                                     -offsets[2] / scales[2]),
                     simpleNN::procs::linear<double>(1.0 / scales[3],
                                                     -offsets[3] / scales[3]),
                     simpleNN::procs::linear<double>(1.0 / scales[4],
                                                     -offsets[4] / scales[4]),
                     simpleNN::procs::linear<double>(1.0 / scales[5],
                                                     -offsets[5] / scales[5]),
                     simpleNN::procs::linear<double>(1.0 / scales[6],
                                                     -offsets[6] / scales[6]),
                     simpleNN::procs::linear<double>(1.0 / scales[7],
                                                     -offsets[7] / scales[7]),
                     simpleNN::procs::linear<double>(1.0 / scales[8],
                                                     -offsets[8] / scales[8]),
                     simpleNN::procs::linear<double>(1.0 / scales[9],
                                                     -offsets[9] / scales[9]),
                     simpleNN::procs::linear<double>(1.0 / scales[10],
                                                     -offsets[10] / scales[10]),
                     simpleNN::procs::linear<double>(1.0 / scales[11],
                                                     -offsets[11] / scales[11]),
                     simpleNN::procs::linear<double>(1.0 / scales[12],
                                                     -offsets[12] / scales[12]),
                     simpleNN::procs::linear<double>(1.0 / scales[13],
                                                     -offsets[13] / scales[13]),
                     simpleNN::procs::linear<double>(1.0 / scales[14],
                                                     -offsets[14] / scales[14]),
                     simpleNN::procs::linear<double>(1.0 / scales[15],
                                                     -offsets[15] / scales[15]),
                     simpleNN::procs::linear<double>(1.0 / scales[16],
                                                     -offsets[16] / scales[16]),
                     simpleNN::procs::linear<double>(1.0 / scales[17],
                                                     -offsets[17] / scales[17]),
                 },
                 biases_l1, weights_l1};
  auto l2 = l2_t{std::array<simpleNN::procs::ReLU, 18>{},
                 std::array<simpleNN::procs::id, 18>{}, biases_l2, weights_l2};
  auto l3 = l3_t{std::array<simpleNN::procs::ReLU, 20>{},
                 std::array<simpleNN::procs::id, 18>{}, biases_l3, weights_l3};
  auto l4 = l4_t{std::array<simpleNN::procs::sigmoid, 1>{},
                 std::array<simpleNN::procs::id, 20>{}, biases_l4, weights_l4};

  auto forward = [=](std::vector<std::array<float, in_feature_dim>> const
                         &input /*(ntracks, in_feature_dim)*/) {
    std::array<float, in_feature_dim> after_l1;
    std::array<float, lat_space_dim> after_l2;
    std::array<float, summed_out_dim> after_l3;
    std::array<float, final_out_dim> output;

    // need to explicitly initialize that one
    std::array<float, lat_space_dim> after_l2_sum{0};

    for (auto const &track : input) {
      l1(after_l1, track);
      l2(after_l2, after_l1);
      for (uint32_t i = 0; i < after_l2.size(); ++i) {
        after_l2_sum[i] += after_l2[i];
      }
    }
    l3(after_l3, after_l2_sum);
    l4(output, after_l3);
    return output[0];
  };

  int nevents = 100000;

  // auto input = generate_events<18>(nevents);
  std::vector<std::array<double, 18>> input {
    {4.16145e-06, 0.000248316, 0.998506, -1,          0.00697444, 0.0023632,
     56469.2,     4074.46,     1,        0.0307174,   3.08098,    -18.1354,
     0.893769,    0.264405,    -8.0837,  1.13453e+08, -0.949334,  -0.58349},
        {3.96179e-07, 0.000853243, 0.0961562, -1,         0.999679,  0.00167727,
         21138.3,     2913.7,      1,         0.0344949,  3.6 6066,  -18.459,
         1.01966,     0.249789,    -5.872,    5.4085e+07, -0.437084, 0.0672832},
        {0.0102866, 0.0247512, 0.446909, -1,          0.0374151, 0.418556,
         105445,    1753.14,   -1,       0.287323,    38.1612,   -10.4177,
         1.01124,   0.279942,  43.0898,  8.11584e+07, 0.877332,  -2.05255},
        {7.04363e-05, 0.00310236, 0.00737565, 0.00165368, 0.00290638,
         0.974478,    8491.56,    1210.35,    1,          0.0450057,
         2.20865,     -18.7976,   0.845506,   0.288387,   -5.6214,
         2.17513e+07, -0.382311,  0.101118},
        {1.38238e-09, 0.000624276, 0.00031553, -1,        1.16483e-05,
         0.996769,    5776.62,     1177.01,    1,         0.0188879,
         0.693043,    -21.1874,    0.860253,   0.4166 47, -2.9362,
         3.03167e+07, -0.941063,   0.463868},
        {4.75524e-08, 0.000819724, 0.000331113, -1,       1.40545e-05,
         0.998439,    7158.83,     884.755,     -1,       0.0394841,
         1.757,       -19.6111,    0.924413,    0.305464, -6.0039,
         2.03051e+07, -0.778914,   -0.0427875},
        {0.00943403,  0.000575735, 0.000404292, -1,       9.04134e-0 5,
         0.986207,    16036.4,     852.815,     1,        0.0272015,
         0.486794,    -20.2554,    0.874799,    0.100535, 14.82,
         1.05955e+07, 0.633083,    -0.889202},
        {2.21626e-05, 0.00525175, 0.148081, -1,          0.0375885, 0.884646,
         25784.9,     781.473,    -1,       0.0673832,   2.03096,   -19.1049,
         0.890449,    0.123144,   -15.5725, 1.96651e+07, 0 .585376, -1.45197},
        {8.09964e-07, 0.00210513, 0.998593, -1,         0.0116762, 0.00220656,
         21147.5,     737.565,    1,        0.0164279,  0.0988145, -20.0137,
         0.914198,    0.138124,   -8.5497,  1.4972e+07, 0.672251,  -1.31146},
        {3.83655e-07, 0.00224006, 0.000743165, 0.00655663, 0.000272149,
         0.985573,    4272.83,    736 .768,    1,          0.0406272,
         1.1389,      -20.7768,   0.901998,    0.226375,   4.2698,
         1.08366e+07, -0.0733212, 0.293922},
        {3.71864e-06, 0.000513313, 5.79281e-05, -1,       1.71766e-06,
         0.996807,    12096.1,     561.568,     -1,       0.0777378,
         1.98462,     -18.9941,    0.906902,    0.162407, 15.8743,
         1.46739e+07, -0.30578,    -1.0252},
        {3.12876e-12, 0.0014026, 0.000123359, -1,       5.22889e-06,
         0.998558,    8681.19,   446.231,     1,        0.023506,
         0.106845,    -20.2814,  0.887172,    0.214375, 10.6361,
         1.20023e+07, -0.501766, -0.923254},
        {0.00701731, 0.0301043, 0.349059, 0.00225153,  0.0956743, 0.490961,
         2943.28,    422.099,   -1,       0.107926,    0.999 654, -21.1293,
         0.901998,   0.430561,  -1.2672,  1.06505e+07, -0.926729, 0.107305},
        {1.43269e-05, 0.0188545, 0.898503, -1,          0.794156,  0.0521339,
         13007.2,     371.607,   -1,       0.088801,    0.781318,  -20.064,
         0.85206,     0.15447,   15.5696,  1.51451e+07, -0.539177, -1.51106},
        {3.47295e-06, 0.00812182, 0.0896 523, 0.00252853,  0.029477,  0.869141,
         2222.7,      353.381,    1,          0.594083,    56.9453,   -9.82345,
         0.926966,    0.261849,   -1.0525,    8.28193e+06, -0.735049, 0.211611},
        {3.05412e-09, 0.21422,   0.000341245, -1,       2.26297e-06,
         0.903079,    5738.14,   325.82,      -1,       0.0183206,
         0.0413588,   -20.0129,  0.962079,    0.239206, -10.2012,
         1.02012e+07, -0.965253, -0.823574},
        {6.543e-08, 0.00123904, 0.0167847, -1,          0.00822362, 0.982515,
         2842.19,   310.34,     -1,        0.190922,    5.97661,    -17.2141,
         0.870786,  0.277893,   -7.4546,   3.39922e+06, 0.309059,   -0.167505},
        {9.20314e-06, 0.0201764, 0.0940246,   0.00245677, 0.0372231,
         0. 83185,    2275.73,   293.333,     -1,         0.0780502,
         1.12743,     -20.5744,  0.933208,    0.106731,   6.9927,
         2.91751e+06, 0.388421,  -0.000403643},
        {8.76893e-07, 0.00146389, 0.0616866, -1,      0.0220997,
         0.915857,    2331.28,    286.922,   -1,      0.0355812,
         0.291034,    -20.653,    0.960163,  0.28545, -5.2274,
         7.50566e+06, -0.9 41581, -0.0469908},
        {2.74101e-06, 0.00370736, 0.00681036, -1,       0.00347699,
         0.985421,    9019.44,    251.741,    -1,       0.714143,
         10.0832,     -15.1185,   0.809925,   0.558705, 19.8694,
         1.13388e+07, -0.813522,  -1.53438},
        {1.00179e-08, 0.0178121, 0.00400644, 0.113066, 0.000213618,
         0.923987,    13018.4,   241.37,     1,        0.403458,
         9.37203,     -13.4402,  0.791198,   0.445959, 47.3097,
         1.34599e+07, -0.349387, -1.94354},
        {4.28763e-08, 0.00861347, 0.00807232, -1,       0.00211855,
         0.981258,    8376.49,    132.452,    -1,       0.503328,
         3.5059,      -19.4934,   0.820225,   0.602823, -23.302,
         7.69015e+06, 0.21091,    -2.10274},
    {
      0.0508483, 0.12434, 0.0370628, -1, 0.0102597, 0.676371, 2122.36, 40.0052,
          1, 1.73856, 2.57983, -21.5788, 0.889513, 1.59957, -53.6894,
          2.13244e+06, 0.652496, -1.92702
    }
  }

  float decision_sum{0};

  auto start = std::chrono::high_resolution_clock::now();
  for (auto const &event : input) {
    decision_sum += forward(event);
  }
  std::cout << decision_sum << std::endl;
  auto end = std::chrono::high_resolution_clock::now();

  std::cout << double(std::chrono::duration_cast<std::chrono::microseconds>(
                          end - start)
                          .count()) /
                   nevents
            << std::endl;
}
