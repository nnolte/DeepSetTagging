## DeepSets for Flavour Tagging


Trained with:    
```sh
 python model_training.py -epochs 100 DTT_MC_Bd2JpsiKst_2015_24_Sim09b_LDST.npz DTT_MC_Bd2JpsiKst_2017_29r2_Sim09i.npz DTT_MC_Bd2JpsiKst_2018_34_Sim09i_DST.npz DTT_MC_Bd2JpsiKst_2016_26_Sim09b-LDST.npz
 ```

Applied to Data with:
```sh
# generate sweights for data application
python calculate_sweights.py preprocessed_datafile.npz
# apply to data
python apply_model_to_data.py -sweights-fname=sweights.npz preprocessed_datafile.npz
```
