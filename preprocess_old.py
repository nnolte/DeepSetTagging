import uproot
import numpy as np

# base_features = ['Tr_T_BPVIP',
#                  'Tr_T_BPVIPCHI2',
#                  'Tr_T_Charge',
#                  'Tr_T_E',
#                  'Tr_T_Eta',
#                  'Tr_T_ISMUON',
#                  'Tr_T_MinIP',
#                  'Tr_T_MinIPChi2',
#                  'Tr_T_P',
#                  'Tr_T_PIDK',
#                  'Tr_T_PIDe',
#                  'Tr_T_PIDmu',
#                  'Tr_T_PIDp',
#                  'Tr_T_PROBNNe',
#                  'Tr_T_PROBNNghost',
#                  'Tr_T_PROBNNk',
#                  'Tr_T_PROBNNmu',
#                  'Tr_T_PROBNNp',
#                  'Tr_T_PROBNNpi',
#                  'Tr_T_PT',
#                  'Tr_T_PX',
#                  'Tr_T_PY',
#                  'Tr_T_PZ',
#                  'Tr_T_Phi',
#                  'Tr_T_THETA',
#                  'Tr_T_TRCHI2DOF',
#                  'Tr_T_TRFITMATCHCHI2',
#                  'Tr_T_TRFITTCHI2',
#                  'Tr_T_TRFITVELOCHI2NDOF',
#                  'Tr_T_TRGHOSTPROB',
#                  'Tr_T_TRPCHI2',
#                  'Tr_T_TRTYPE',
#                  'Tr_T_TrFIRSTHITZ',
#                  'Tr_T_TrFITTCHI2NDOF',
#                  'Tr_T_VeloCharge',
#                  'Tr_T_AALLSAMEBPV',
#                  'Tr_T_ACHI2DOCA',
#                  'Tr_T_ADOCA',
#                  'Tr_T_Sum_of_trackp',
#                  'Tr_T_Sum_of_trackpt',
#                  'Tr_T_Cone_asym_Pt',
#                  'Tr_T_Cone_asym_P',
#                  'Tr_T_ConIso_p_ult',
#                  'Tr_T_ConIso_pt_ult',
#                  'Tr_T_Ntr_incone',
#                  'Tr_T_Mother_VtxChi2',
#                  'Tr_T_IPCHI2_trMother',
#                  'Tr_T_IP_trMother',
#                  'Tr_T_IP_trPUS',
#                  'Tr_T_x',
#                  'Tr_T_y',
#                  'Tr_T_xfirst',
#                  'Tr_T_yfirst',
#                  'Tr_T_SumBDT_sigtr',
#                  'Tr_T_MinBDT_sigtr',
#                  'Tr_T_NbTrNonIso_sigtr',
#                  'Tr_T_SumBDT_ult',
#                  'Tr_T_SumMinBDT_ult',
#                  'Tr_T_MinBDT_ult',
#                  'Tr_T_NbNonIsoTr_ult',
#                  'Tr_T_NbNonIsoTr_MinBDT_ult',
#                  'Tr_T_Best_PAIR_VCHI2',
#                  'Tr_T_Best_PAIR_D',
#                  'Tr_T_Best_PAIR_DCHI2',
#                  'Tr_T_Best_PAIR_M_fromiso']

base_features = [
    "Tr_T_PROBNNe",
    "Tr_T_PROBNNghost",
    "Tr_T_PROBNNk",
    "Tr_T_PROBNNmu",
    "Tr_T_PROBNNp",
    "Tr_T_PROBNNpi",
    "Tr_T_P",
    "Tr_T_PT",
    "Tr_T_Charge",
    "Tr_T_BPVIP",
    "Tr_T_BPVIPCHI2",
    "Tr_T_SumBDT_ult",
    "Tr_T_VeloCharge",
    "Tr_T_IP_trMother",
]

extras = [
    "Tr_T_TrFIRSTHITZ",
    "B_ENDVERTEX_Z",
    "B_OWNPV_Z",
    "B_PX",
    "B_PY",
    "B_PZ",
    "B_PE",
    "Tr_T_PX",
    "Tr_T_PY",
    "Tr_T_PZ",
    "Tr_T_E",
    "B_TRUEID",
    "B_LOKI_PHI",
    "Tr_T_Phi",
    "B_LOKI_ETA",
    "Tr_T_Eta",
    "Tr_ORIG_FLAGS",
]

cols = base_features + extras


import sys

# rootfile = '/home/chasse/ml_stuff/ft_testing/DTT_MC_Bd2JpsiKst_2015_24_Sim09b_LDST.root'
rootfile = sys.argv[1]


tree = uproot.open(rootfile)["DecayTree"]

data = tree.arrays(cols, library="np")


# number of tracks per event
evt_sizes = [len(x) for x in data["Tr_T_P"]]
evt_idx = np.cumsum([0] + evt_sizes, dtype=np.int32)
# array of (start_idx, end_idx) per event to find tracks inside the flattened arrays below
borders = np.array(list(zip(evt_idx[:-1], evt_idx[1:])))
# total number of tracks in all events
ntracks_total = sum(evt_sizes)


# vector that maps each track to it original event
# needed to later recalculate borders after filtering
# and remove empty events
tr_2_evt_idx = np.zeros(ntracks_total, dtype=np.int64)

for i, (b, e) in enumerate(borders):
    tr_2_evt_idx[b:e] = i


n_base = len(base_features)


out_data = np.zeros((ntracks_total, n_base + 5), dtype=np.float32)

for idx, name in enumerate(base_features):
    print(idx, name)
    out_data[:, idx] = np.concatenate(data[name])


# create B_Class
tags = np.array(data["B_TRUEID"])
# diff_z
out_data[:, n_base] = np.concatenate(data["Tr_T_TrFIRSTHITZ"] - data["B_OWNPV_Z"])
out_data[:, n_base + 1] = np.concatenate(
    data["Tr_T_TrFIRSTHITZ"] - data["B_ENDVERTEX_Z"]
)
# cos_diff_phi
out_data[:, n_base + 2] = np.cos(np.concatenate(data["B_LOKI_PHI"] - data["Tr_T_Phi"]))
# diff_eta
out_data[:, n_base + 3] = np.concatenate(data["B_LOKI_ETA"] - data["Tr_T_Eta"])


# use float32 to save space
b_p4 = np.array(
    [-1, -1, -1, 1]
    * np.column_stack((data["B_PX"], data["B_PY"], data["B_PZ"], data["B_PE"])),
    dtype=np.float32,
)

t_p4 = np.column_stack([data[x] for x in ["Tr_T_PX", "Tr_T_PY", "Tr_T_PZ", "Tr_T_E"]])


# proj
out_data[:, n_base + 4] = np.concatenate(np.sum(t_p4 * b_p4, 1))


tr_orig_flags = np.concatenate(data["Tr_ORIG_FLAGS"])

mask = tr_orig_flags != 0
tr_orig_flags = tr_orig_flags[mask]

out_data = out_data[mask]


tr_orig_flags[(tr_orig_flags > 50) | (tr_orig_flags == -1) | (tr_orig_flags == 5)] = 0
tr_orig_flags[tr_orig_flags == 4] = 3


# which events still have # tracks > 0 and how many tracks does each event have?
kept_evts, counts = np.unique(tr_2_evt_idx[mask], return_counts=True)
# just the borders of each event
evt_borders = np.cumsum(np.concatenate(([0], counts)))

# filter out empty events
tags = tags[kept_evts]


np.savez(
    "np_data",
    Tr_ORIG_FLAGS=tr_orig_flags,
    B_TRUEID=tags,
    features=out_data,
    evt_borders=evt_borders,
)

