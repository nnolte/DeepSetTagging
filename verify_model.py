#!/usr/bin/env python
# coding: utf-8

import numpy as np
import torch
import matplotlib.pyplot as plt
import joblib
from model import BaselineModel
import uproot3 as u
from feature_names import features as feature_names

label_name = "B_IFT_InclusiveTagger_MVAOUT"

feature_names = [f.replace("Bd_", "") for f in feature_names]
df = u.open("DTT.root")["TaggingTest/DecayTree"].pandas.df(feature_names + [label_name])
features = df[feature_names].to_numpy()
label = df[label_name][:,0].to_numpy()

model_fname = "output/model.pt"
scaler_fname = "output/model_scaler.bin"
device = torch.device("cpu")

scaler = joblib.load(f"{scaler_fname}")
features = scaler.transform(features)

feat = torch.tensor(features).float().to(device)

model = BaselineModel(model_fname).to(device)
model.eval()
idx = [i for (i,j) in df.index]
idx = torch.tensor(idx).int().to(device)
out = torch.nn.functional.sigmoid(model(feat, idx)).detach().numpy()

plt.hist(out, bins=100, alpha=.3, label="pytorch")
plt.hist(label, bins=100, alpha=1, histtype="step", label="davinci")
plt.legend()
plt.show()
