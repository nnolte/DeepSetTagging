import torch
import torch.nn as nn

def run_ds(x, idx, l1, l2, l3, l4, ls_dim):
    x = l1(x)
    x = nn.functional.relu(x)
    x = l2(x)
    tmp = nn.functional.relu(x)

    # the last entry of idx is the index for the last entry
    # so +1 is equal to number of events
    x = torch.zeros(idx[-1] + 1, ls_dim, device=x.device)
    x.index_add_(0, idx, tmp)

    x = l3(x)
    x = nn.functional.relu(x)
    x = l4(x)
    return x


class BaselineModel(nn.Module):
    def __init__(
        self, load_weights_path=None, lat_space_dim=18, in_feature_dim=18, cpu=False
    ):
        super().__init__()
        self.latent_space_dim = lat_space_dim
        self.in_feature_dim = in_feature_dim

        self.l1 = nn.Linear(self.in_feature_dim, self.in_feature_dim)
        self.l2 = nn.Linear(self.in_feature_dim, self.latent_space_dim)

        self.l3 = nn.Linear(self.latent_space_dim, 20)
        self.l4 = nn.Linear(20, 1)

        if load_weights_path != None:
            self.load_state_dict(
                torch.load(
                    load_weights_path, map_location=torch.device("cpu") if cpu else None
                )
            )

    def forward(self, x, idx):
        return run_ds(x, idx, self.l1, self.l2, self.l3, self.l4, self.latent_space_dim)



class TrackCatNetwork(nn.Module):
    def __init__(self, input_dim = 18):
        super().__init__()
        # deepset part
        n_classes = 4
        self.ds_latent_space_dim = input_dim
        tc_ds_output_dim = 16
        self.l1_tc = nn.Linear(input_dim, 16)
        self.l2_tc = nn.Linear(16, self.ds_latent_space_dim)
        self.l3_tc = nn.Linear(self.ds_latent_space_dim, 16)
        self.l4_tc = nn.Linear(16, tc_ds_output_dim)

        self.l1_tag = nn.Linear(input_dim + n_classes, 16)
        self.l2_tag = nn.Linear(16, self.ds_latent_space_dim)
        self.l3_tag = nn.Linear(self.ds_latent_space_dim, 16)
        self.l4_tag = nn.Linear(16, 1)

        # track classification part
        self.t1 = nn.Linear(input_dim, 16)
        self.t2 = nn.Linear(16, 16)
        self.t3 = nn.Linear(16 + tc_ds_output_dim, 16)
        self.t4 = nn.Linear(16, 16)
        self.t5 = nn.Linear(16, n_classes)
        self.dropout = nn.Dropout(p=0.1)

    def forwardTCDeepSet(self, x, idx):
        return run_ds(
            x,
            idx,
            self.l1_tc,
            self.l2_tc,
            self.l3_tc,
            self.l4_tc,
            self.ds_latent_space_dim,
        )

    def forwardTCNN(self, x):
        x = self.t1(x)  # [ntracks, 32]
        x = nn.functional.relu(x)
        return self.t2(x)  # [ ntracks, latent_space]

    def concatAndForwardTC(self, tc, ds, idx):
        # x is now one entry per event. extend this to every track (ntracks * event latent space)
        ds = ds.repeat_interleave(idx.bincount(), dim=0)
        # now add the initial x (x0) to it
        x = torch.hstack((tc, ds))  # [ntracks, latent_space + nfeatures]
        x = self.t3(x)
        x = self.dropout(x)
        x = nn.functional.relu(x)
        x = self.t4(x)
        x = nn.functional.relu(x)
        x = self.t5(x)  # [ntracks, nclasses]
        return nn.functional.softmax(x, dim=1)  # [ntracks, nclasses]

    def forwardTaggingDeepSet(self, tc, x, idx):
        x = torch.hstack((tc, x))
        return run_ds(
            x,
            idx,
            self.l1_tag,
            self.l2_tag,
            self.l3_tag,
            self.l4_tag,
            self.ds_latent_space_dim,
        )

    def forward(self, x, idx):
        tcds = self.forwardTCDeepSet(x, idx)
        tc = self.forwardTCNN(x)
        tc = self.concatAndForwardTC(tc, tcds, idx)
        tag = self.forwardTaggingDeepSet(tc, x, idx)
        return tc, tag
