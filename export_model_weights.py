from model import BaselineModel
import json
import joblib
from feature_names import features

from sys import argv
try:
    model_name = argv[1]
    scaler_name = argv[2]
except IndexError:
    print("Please specify model.pt path and scaler.bin path in argv[1] and argv[2]")
    exit()

model = BaselineModel(model_name, cpu=True)
scaler = joblib.load(scaler_name)


df = dict()
for i,layer in enumerate((model.l1, model.l2, model.l3, model.l4)):
  df[f'weight{i+1}'] = tuple(map(float, layer.weight.detach().numpy().flatten()))
  df[f'bias{i+1}'] = tuple(map(float, layer.bias.detach().numpy().flatten()))

df["features"] = tuple(features)
df["scales"] = tuple(map(float, scaler.scale_))
df["offsets"] = tuple(map(float, scaler.center_))

with open("output/model.json", "w") as outfile:
  json.dump(df, outfile)
