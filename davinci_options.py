from Configurables import (DecayTreeTuple,
                           EventTuple,
                           MCDecayTreeTuple,
                           LoKi__Hybrid__MCTupleTool,
                           DaVinci,
                           TrackScaleState,
                           TrackSmearState,
                           FilterDesktop,
                           CondDB,
                           TaggerMuonTool,
                           CallgrindProfile,
                           BTaggingTool,
                           MessageSvc,
                           TupleToolDecayTreeFitter,
                           TupleToolMCBackgroundInfo,
                           BackgroundCategory,
                           GaudiSequencer,
                           CheckPV,
                           LHCbApp)
from Gaudi.Configuration import INFO, DEBUG, WARNING
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand, MergedSelection
from GaudiConf import IOHelper

from DecayTreeTuple.Configuration import *

from FlavourTagging.Tunings import applyTuning

#########################
# DAVINCI CONFIGURATION #
#########################
DaVinci().TupleFile  = "DTT.root"
DaVinci().DataType   = "2016"
DaVinci().Simulation = True
DaVinci().InputType  = "LDST"
DaVinci().EvtMax     = 10000
DaVinci().RootInTES  = "/Event"

stripping_line = "BetaSBu2JpsiKDetachedLine"
prefix = "AllStreams" if DaVinci().Simulation else "Dimuon"

restrip_years = [] # ["2016", "2017"]
do_restripping = DaVinci().Simulation and DaVinci().DataType in restrip_years

if DaVinci.Simulation:
    if do_restripping:
        ###############
        # RESTRIPPING #
        ###############
        from Configurables import EventNodeKiller, ProcStatusCheck
        from StrippingArchive import strippingArchive
        from StrippingConf.Configuration import StrippingConf, StrippingStream
        from StrippingArchive.Utils import buildStreams
        from StrippingSettings.Utils import strippingConfiguration
        # Remove stripping lines
        event_node_killer = EventNodeKiller("StripKiller")
        event_node_killer.Nodes = ["/Event/AllStreams", "/Event/Strip"]

        strip = {
            "2015" : "stripping24r2",
            "2016" : "stripping28r2",
            "2017" : "stripping29r2",
            "2018" : "stripping34",
        }[DaVinci().DataType]

        strippingArchive()

        print("strip = ", strip)
        streams = buildStreams(stripping = strippingConfiguration(strip),
                               archive = strippingArchive(strip))
        custom_stream = StrippingStream('CustomStream')
        custom_line = "Stripping" + stripping_line

        for stream in streams:
            for sline in stream.lines:
                if sline.name() == custom_line:
                    custom_stream.appendLines([sline])

        filterBadEvents = ProcStatusCheck()
        sc = StrippingConf(Streams = [custom_stream],
                           MaxCandidates = 2000,
                           AcceptBadEvents = False,
                           BadEventSelection = filterBadEvents)
        DaVinci().appendToMainSequence([event_node_killer, sc.sequence()])
        
    LHCbApp().DDDBtag   = "dddb-20170721-3"
    LHCbApp().CondDBtag = "sim-20170721-2-vc-md100"

#############
# SELECTION #
#############
inputBu2JpsiK = DataOnDemand(Location=prefix + "/Phys/BetaSBu2JpsiKDetachedLine/Particles")

cutsBu2JpsiK_mu = "INGENERATION( (ABSID=='mu+') & (PT>500*MeV) & (PIDmu>0),2 )"
cutsBu2JpsiK_jpsi = "INTREE( (ID=='J/psi(1S)') & (M>3036*MeV) & (M<3156*MeV) )"
cutsBu2JpsiK_k = "INGENERATION( (ABSID=='K+') & (PT>1000*MeV) & (P>10000*MeV) & (PIDK>0),1 )"
cutsBu2JpsiK_b = "(M>5000*MeV) & (M<6000*MeV) & (BPVIPCHI2()<25) & (BPVLTIME('PropertimeFitter/properTime:PUBLIC')>0.2*ps)"

cutsBu2JpsiK = "(" + " & ".join([cutsBu2JpsiK_mu,
                                 cutsBu2JpsiK_jpsi, 
                                 cutsBu2JpsiK_k,
                                 cutsBu2JpsiK_b]) + ")"

filterBu2JpsiK = FilterDesktop("filterBu2JpsiK", Code=cutsBu2JpsiK)

selBuJpsiK = Selection("selBuJpsiK",
                       Algorithm=filterBu2JpsiK,
                       RequiredSelections=[inputBu2JpsiK])

seqBuJpsiK = SelectionSequence("seqBuJpsiK", TopSelection=selBuJpsiK)

DaVinci().appendToMainSequence([seqBuJpsiK.sequence()])


#################
# DEFINE NTUPLE #
#################
ntuple = DecayTreeTuple("TaggingTest")
if do_restripping:
    ntuple.Inputs = ['/Event/Phys/' + stripping_line + '/Particles']
else:
    ntuple.Inputs = [prefix + '/Phys/' + stripping_line + '/Particles']

ntuple.Decay = "[B+ -> ^(J/psi(1S) -> ^mu+ ^mu-) ^K+]CC"
ntuple.addBranches({'B'       : "^[B+ -> (J/psi(1S) -> mu+ mu-) K+]CC",
                    'Jpsi'    : "[B+ -> ^(J/psi(1S) -> mu+ mu-) K+]CC",
                    'muminus' : "[B+ -> (J/psi(1S) -> ^mu+ mu-) K+]CC",
                    'muplus'  : "[B+ -> (J/psi(1S) -> mu+ ^mu-) K+]CC",
                    'K'       : "[B+ -> (J/psi(1S) -> mu+ mu-) ^K+]CC"})
ntuple.ReFitPVs = False

loki_variables = { "LOKI_ENERGY"       : "E",
                   "LOKI_ETA"          : "ETA",
                   "LOKI_PHI"          : "PHI",
                   "LOKI_DTF_CHI2NDOF" : "DTF_CHI2NDOF( True )",
                   "Vtx_Chi2NDOF"      : "VFASPF(VCHI2PDOF)",
                   "LOKI_FDS"          : "BPVDLS" }

TriggerLines = {
    '2016': {
        'L0':   ['L0PhysicsDecision',
                 'L0MuonDecision',
                 'L0DiMuonDecision',
                 'L0MuonHighDecision',
                 'L0HadronDecision',
                 'L0ElectronDecision',
                 'L0PhotonDecision'],
        'HLT1': ['Hlt1GlobalDecision',
                 'Hlt1DiMuonHighMassDecision',
                 'Hlt1TrackMVADecision',
                 'Hlt1TwoTrackMVADecision',
                 'Hlt1TrackMuonDecision',
                 'Hlt1TrackMuonMVADecision'],
        'HLT2': ['Hlt2GlobalDecision',
                 'Hlt2DiMuonJPsiDecision',
                 'Hlt2DiMuonDetachedJPsiDecision'],
    },
    '2017': {
        'L0':   ['L0PhysicsDecision',
                 'L0MuonDecision',
                 'L0DiMuonDecision',
                 'L0MuonHighDecision',
                 'L0HadronDecision',
                 'L0ElectronDecision',
                 'L0PhotonDecision'],
        'HLT1': ['Hlt1GlobalDecision',
                 'Hlt1DiMuonHighMassDecision',
                 'Hlt1TrackMVADecision',
                 'Hlt1TwoTrackMVADecision',
                 'Hlt1TrackMuonDecision',
                 'Hlt1TrackMuonMVADecision'],
        'HLT2': ['Hlt2GlobalDecision',
                 'Hlt2DiMuonJPsiDecision',
                 'Hlt2DiMuonDetachedJPsiDecision'],
    }
}

triggerlists = TriggerLines[DaVinci().DataType]
trigger_lines = triggerlists['L0'] + triggerlists['HLT1'] + triggerlists['HLT2']

##############
# TUPLETOOLS #
##############

# TupleToolPrimaries
tt_prim = ntuple.addTupleTool('TupleToolPrimaries')
tt_prim.Verbose = False
# TupleToolPropertime
tt_proptime = ntuple.addTupleTool('TupleToolPropertime')
tt_proptime.Verbose = False
# TupleToolRecoStats
tt_recostats = ntuple.addTupleTool("TupleToolRecoStats")
# TupleToolTrackInfo
tt_trackinfo = ntuple.addTupleTool('TupleToolTrackInfo')
tt_trackinfo.Verbose = True
# TupleToolTrackPosition
tt_trackpos = ntuple.addTupleTool('TupleToolTrackPosition')
tt_trackpos.Z = 7500.
# TupleToolANNPID
tt_pid = ntuple.addTupleTool("TupleToolANNPID/TTPidAnn")
tt_pid.Verbose = True
tt_loki_general = ntuple.addTupleTool("LoKi::Hybrid::TupleTool/tt_loki_general")
tt_loki_general.Variables = loki_variables

###########
# TRIGGER #
###########
ntuple.addTupleTool('TupleToolDecay/B')
tt_tistos_b = ntuple.B.addTupleTool("TupleToolTISTOS")
tt_tistos_b.VerboseL0 = True
tt_tistos_b.VerboseHlt1 = True
tt_tistos_b.VerboseHlt2 = True
tt_tistos_b.FillL0 = True
tt_tistos_b.FillHlt1 = True
tt_tistos_b.FillHlt2 = True
tt_tistos_b.OutputLevel = INFO
tt_tistos_b.TriggerList = trigger_lines

# DTF
FitDaughtersConst = ntuple.B.addTupleTool("TupleToolDecayTreeFitter/FitDaughtersConst")
FitDaughtersConst.Verbose = True
FitDaughtersConst.UpdateDaughters = True
FitDaughtersConst.daughtersToConstrain = ["J/psi(1S)"]

###########
# MC INFO #
###########

if DaVinci().Simulation:
    tt_mct = ntuple.addTupleTool("TupleToolMCTruth", name = ("TupleToolMCTruth"))
    tt_mct.ToolList = ["MCTupleToolReconstructed"]

    # add MCTupleTools
    tt_mct.ToolList += ["MCTupleToolKinematic",
                        "MCTupleToolHierarchy",
                        "MCTupleToolPID"]
    ntuple.addTupleTool("TupleToolMCBackgroundInfo", name =("TupleToolMCBackgroundInfo"))
    ntuple.TupleToolMCBackgroundInfo.addTool(BackgroundCategory("BackgroundCategory"))


checkPV = CheckPV("CheckForOnePV")
checkPV.MinPVs = 1

if DaVinci().Simulation:
    ###################################
    # CONFIGURE MCTUPLE & EVENT TUPLE #
    ###################################
    evtTuple = EventTuple()
    evtTuple.ToolList += ["TupleToolEventInfo", "TupleToolTrigger"]

    mctuple = MCDecayTreeTuple("MCtuple")
    mctuple.Decay = "[B+ => ^(J/psi(1S) => ^mu+ ^mu-) ^K+]CC"

    mctuple.ToolList = ["MCTupleToolHierarchy",
                        "MCTupleToolPID",
                        "MCTupleToolReconstructed",
                        "MCTupleToolDecayType",
                        "MCTupleToolKinematic",
                        "MCTupleToolEventType",
                        "MCTupleToolInteractions",
                        "LoKi::Hybrid::MCTupleTool/LoKi_Photos"]

    LoKi_Photos = LoKi__Hybrid__MCTupleTool("LoKi_Photos")
    LoKi_Photos.Variables = { "nPhotos"  : "MCNINTREE ( ('gamma'==MCABSID) )" }
    mctuple.addTool(LoKi_Photos)


###################
# FLAVOUR TAGGING #
###################
tt_tagging = ntuple.B.addTupleTool("TupleToolTagging/old")
tt_tagging.Verbose = True
tt_tagging.AddMVAFeatureInfo = False
tt_tagging.AddTagPartsInfo = False
tt_tagging.OutputLevel = INFO
btagtool = tt_tagging.addTool(BTaggingTool, name = "MyBTaggingTool2")
applyTuning(btagtool, tuning_version="Summer2017Optimisation_v4_Run2")
tt_tagging.TaggingToolName = btagtool.getFullName()

# TupleToolTagging: IFT
tt_tagging_ift = ntuple.B.addTupleTool("TupleToolTagging/ift")
tt_tagging_ift.Verbose = True
tt_tagging_ift.AddMVAFeatureInfo = True
tt_tagging_ift.AddTagPartsInfo = True
tt_tagging_ift.OutputLevel = INFO
btagtool_ift = tt_tagging_ift.addTool(BTaggingTool, name = "IftBTaggingTool")
btagtool_ift.TaggingParticleLocation = "Phys/TaggingIFTTracks"
tt_tagging_ift.allConfigurables["ToolSvc.InclusiveTagger"].setProp("ClassifierVersion", "IFT_Bd_v031121")
tt_tagging_ift.allConfigurables["ToolSvc.InclusiveTagger"].setProp("WeightFileOverride", "model.json")
tt_tagging_ift.ExtraName = "IFT"
applyTuning(btagtool_ift, tuning_version="Summer2019Optimisation_v1_Run2")
tt_tagging_ift.TaggingToolName = btagtool_ift.getFullName()

######################
# SMEARING & SCALING #
######################

DaVinci().UserAlgorithms = [ ntuple ]

if DaVinci().Simulation:
    smear = TrackSmearState('Smear')
    DaVinci().UserAlgorithms += [smear, evtTuple, mctuple]
else:
    scaler = TrackScaleState('Scaler')
    DaVinci().UserAlgorithms += [scaler]


MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"
IOHelper('ROOT').inputFiles([
    'root://x509up_u5036@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2016/ALLSTREAMS.LDST/00095724/0000/00095724_00000065_7.AllStreams.ldst'
], clear=True)

