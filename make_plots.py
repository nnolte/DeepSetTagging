import matplotlib.pyplot as plt
import uproot3 as u
from sklearn.metrics import roc_auc_score, accuracy_score
import numpy as np
from sys import argv
from IPython import embed

do_make_plots = False

asyms = []
accs = []
aucs = []

for model in argv[1:]:
    df = u.open(model)["DecayTree"].pandas.df()

    is_B = df["B_TRUEID"] > 0

    best_split = .5 # df["pred"].mean()
    aucs.append(roc_auc_score(is_B, df["pred"]))
    accs.append(accuracy_score(is_B, df["pred"] > best_split))

    def get_asym(b_pred, anti_b_pred):
        return (b_pred - anti_b_pred) / (b_pred + anti_b_pred)

    n_b = is_B.sum()
    n_anti_b = len(is_B) - n_b

    n_pred_b = (df["pred"] > best_split).sum()
    n_pred_anti_b = (df["pred"] < best_split).sum()

    asyms.append(get_asym(n_pred_b, n_pred_anti_b))


    if do_make_plots:
        h1, _, _ = plt.hist(df[is_B]["pred"], bins=100, label="B", alpha=0.5, color="red")
        h2, _, _ = plt.hist(df[~is_B]["pred"], bins=100, label="antiB", alpha=0.5, color="blue")
        plt.vlines([.5], [0], [max(max(h1), max(h2))])
        plt.show()

embed()
